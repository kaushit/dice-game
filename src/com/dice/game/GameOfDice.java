package com.dice.game;

import java.util.*;

public class GameOfDice {
    private static final int MAX_DICE_VALUE = 6;

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: java GameOfDice <number_of_players> <points_to_accumulate>");
            return;
        }

        int numPlayers = Integer.parseInt(args[0]);
        int pointsToAccumulate = Integer.parseInt(args[1]);

        List<Player> players = new ArrayList<>();
        for (int i = 1; i <= numPlayers; i++) {
            players.add(new Player("Player-" + i));
        }

        Collections.shuffle(players); // Randomly assign the order in which players will roll the dice

        CircularLinkedList<Player> activePlayers = new CircularLinkedList<>();

        for (Player player : players) {
            activePlayers.add(player);
        }

        CircularLinkedList<Player>.CircularLinkedListIterator activePlayerIterator = activePlayers.iterator();
        Player currentPlayer = activePlayerIterator.next();
        Scanner scanner = new Scanner(System.in);
        while (!activePlayers.isEmpty()) {
            if (currentPlayer.isPenalty()) {
                System.out.println(currentPlayer.getName() + ", You hava penalty! Skipping your turn");
                currentPlayer.resetPenalty();
                currentPlayer = activePlayerIterator.next();
                continue;
            }
            System.out.println(currentPlayer.getName() + ", it's your turn (press 'r' to roll the dice)");
            scanner.nextLine();

            int diceValue = rollDice();
            System.out.println("You rolled a " + diceValue);

            if (diceValue == 6) {
                System.out.println("Congratulations! You get another chance to roll again.");
                currentPlayer.addPoints(diceValue);
                continue;
            }

            if (diceValue == 1) {
                currentPlayer.addPoints(diceValue);
                currentPlayer.incrementConsecutiveOnes();
                if (currentPlayer.getConsecutiveOnes() == 2) {
                    System.out.println("Oops! You rolled '1' two consecutive times. Skipping your next turn.");
                    currentPlayer.resetConsecutiveOnes();
                    currentPlayer.setPenalty();
                }
            } else {
                currentPlayer.resetConsecutiveOnes();
                currentPlayer.addPoints(diceValue);
            }

            if (currentPlayer.getPoints() >= pointsToAccumulate) {
                activePlayerIterator.remove(currentPlayer);
                currentPlayer.setPoints(pointsToAccumulate);
                System.out.println(currentPlayer.getName() + " completed the game and got rank " + (players.indexOf(currentPlayer) + 1));
            }
            Collections.sort(players); // Sort players based on points
            printRankTable(players);
            currentPlayer = activePlayerIterator.next();
        }
    }

    private static int rollDice() {
        return new Random().nextInt(MAX_DICE_VALUE) + 1;
    }

    private static void printRankTable(List<Player> players) {
        System.out.println("Current Rank Table:");
        for (Player player : players) {
            System.out.println(player.getName() + " - " + player.getPoints() + " points");
        }
    }
}