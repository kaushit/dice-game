package com.dice.game;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularLinkedList<E> implements Iterable<E> {

    private static class Node<E> {
        E data;
        Node<E> next;

        Node(E data) {
            this.data = data;
            this.next = null;
        }
    }

    private Node<E> head;
    private int size;

    public CircularLinkedList() {
        head = null;
        size = 0;
    }

    public void add(E data) {
        Node<E> newNode = new Node<>(data);
        if (head == null) {
            head = newNode;
            head.next = head;
        } else {
            newNode.next = head.next;
            head.next = newNode;
        }
        size++;
    }

    public boolean remove(E data) {
        if (head == null) {
            return false;
        }

        Node<E> current = head;
        if (data == head.data){
            if(head.next != head){
                Node<E> newHead = head.next;
                while (current.next != head) {
                    current = current.next;
                }
                current.next = newHead;
                head = newHead;
                size--;
            } else {
                head = null;
                size = 0;
            }
            return true;
        }

        while (current.next != head) {
            if (current.next.data.equals(data)) {
                current.next = current.next.next;
                size--;
                return true;
            }
            current = current.next;
        }

        return false;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public CircularLinkedListIterator iterator() {
        return new CircularLinkedListIterator();
    }

    public class CircularLinkedListIterator implements Iterator<E> {
        private Node<E> current;
        private boolean removed;

        private Node<E> previous;
        CircularLinkedListIterator() {
            current = head;
            removed = false;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            E data = current.data;
            current = current.next;
            return data;
        }

        public void remove(E data) {
            if (current == null || removed) {
                throw new IllegalStateException();
            }

            CircularLinkedList.this.remove(data);
        }
    }
}
