package com.dice.game;

class Player implements Comparable<Player> {
    private String name;
    private int points;
    private int consecutiveOnes;
    private boolean penalty;

    public boolean isPenalty() {
        return penalty;
    }

    public void setPenalty() {
        this.penalty = true;
    }

    public void resetPenalty(){
        this.penalty = false;
    }

    public Player(String name) {
        this.name = name;
        this.points = 0;
        this.consecutiveOnes = 0;
        this.penalty = false;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void addPoints(int points) {
        this.points += points;
    }


    public void incrementConsecutiveOnes() {
        this.consecutiveOnes++;
    }

    public void resetConsecutiveOnes() {
        this.consecutiveOnes = 0;
    }

    public int getConsecutiveOnes() {
        return consecutiveOnes;
    }

    @Override
    public int compareTo(Player other) {
        return Integer.compare(other.points, this.points); // Descending order based on points
    }
}
